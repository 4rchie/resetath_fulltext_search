﻿using SearchEngine.LuceneNET;
using SearchEngine.Shared;
using SearchEngine.Sql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngine
{
    public class Searcher
    {
        private bool _askForRebuildIndex;

        public event Action<SearchResult> SearchCompleted;

        public Searcher(bool askForRebuildIndex = true)
        {
            _askForRebuildIndex = askForRebuildIndex;
        }

        public SearchResult Search(string phrase, eSearcher searcherType)
        {
            ISearcher searcher;

            switch (searcherType)
            {
                case eSearcher.Lucene:
                    searcher = new LuceneSearcher(_askForRebuildIndex);
                    break;
                case eSearcher.Sql:
                default:
                    searcher = new SqlSearcher();
                    break;
            }

            return searcher.Search(phrase);
        }

        public Task SearchAsync(string phrase, eSearcher searcherType)
        {
            return Task.Run(() =>
            {
                var result = Search(phrase, searcherType);
                SearchCompleted?.Invoke(result);
            });
        }
    }
}
