﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lucene;
using Lucene.Net.Store;
using Lucene.Net.Analysis;
using Lucene.Net.Index;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Db;

namespace SearchEngine.LuceneNET
{
    public class LuceneService
    {
        public LuceneService(bool askForRebuildIndex)
        {
            if (CheckIsIndexExist())
            {
                if (askForRebuildIndex)
                    AskForRebuildIndex();
            }
            else
                BuildIndex();
        }

        private void BuildIndex()
        {
            var indexBuilder = new IndexBuilder();

            indexBuilder.BuildIndexCompleted += () => Console.WriteLine("*Utworzono indeks.\n");
            indexBuilder.BuildIndex();
        }

        private void AskForRebuildIndex()
        {
            Console.Write("#Czy przebudować indeks? [T/n]: ");
            var decision = Console.ReadKey();
            Console.WriteLine("\n");

            if (decision.Key == ConsoleKey.T)
                BuildIndex();
        }

        private bool CheckIsIndexExist()
        {
            return System.IO.Directory.Exists(IndexBuilder.IndexFolderPath);
        }

        public IEnumerable<Articles> Search(string phrase)
        {
            using (var analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30))
            {
                var parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_30, "title", analyzer);
                var searchquery = string.Format("title:{0} OR description:{0}", phrase);
                var query = parser.Parse(searchquery);

                using (var indexDirectory = IndexBuilder.GetIndexDirectory())
                {
                    using (var searcher = new IndexSearcher(IndexReader.Open(indexDirectory, true)))
                    {
                        var collector = TopScoreDocCollector.Create(100, true);

                        searcher.Search(query, collector);
                        
                        foreach (var item in collector.TopDocs().ScoreDocs)
                        {
                            var doc = searcher.Doc(item.Doc);

                            yield return new Articles
                            {
                                Id = Guid.Parse(doc.Get("id")),
                                Title = doc.Get("title"),
                                Description = doc.Get("description"),
                                Score = item.Score
                            };
                        }
                    }
                }
            }
        }

        public IEnumerable<Articles> SearchBooleanTerms(string phrase)
        {
            var booleanQuery = new BooleanQuery();
            var titleTerm = new TermQuery(new Term("title", phrase));
            var descriptionTerm = new TermQuery(new Term("description", phrase));

            booleanQuery.Add(titleTerm, Occur.SHOULD);
            booleanQuery.Add(descriptionTerm, Occur.SHOULD);

            using (var indexDirectory = IndexBuilder.GetIndexDirectory())
            {
                using (var searcher = new IndexSearcher(IndexReader.Open(indexDirectory, true)))
                {
                    var results = searcher.Search(booleanQuery, 100);
                    var resultsOrdered = results.ScoreDocs
                        .OrderByDescending(n => n.Score)
                        .ToList();

                    foreach (var item in resultsOrdered)
                    {
                        var doc = searcher.Doc(item.Doc);

                        yield return new Articles
                        {
                            Title = doc.Get("title"),
                            Description = doc.Get("description")
                        };
                    }
                }
            }
        }
    }
}
