﻿using System;
using SearchEngine.Shared;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SearchEngine.LuceneNET
{
    public class LuceneSearcher : ISearcher
    {
        LuceneService _service;

        public LuceneSearcher(bool askForRebuildIndex = true)
        {
            _service = new LuceneService(askForRebuildIndex);
        }

        public event Action<SearchResult> SearchCompleted;

        public SearchResult Search(string phrase)
        {
            var sw = new Stopwatch();
            var result = new SearchResult();
            
            sw.Start();
            var responseData = _service.Search(phrase).ToList();
            sw.Stop();

            result.SearcherName = GetType().Name;
            result.Articles = responseData;
            result.ExecutionTime = sw.ElapsedMilliseconds;

            return result;
        }

        public Task SearchAsync(string phrase)
        {
            return Task.Run(() =>
            {
                var result = Search(phrase);
                SearchCompleted?.Invoke(result);
            });
        }
    }
}