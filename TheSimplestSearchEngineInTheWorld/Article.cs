﻿using System;

namespace TheSimplestSearchEngineInTheWorld
{
    internal class Article
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}